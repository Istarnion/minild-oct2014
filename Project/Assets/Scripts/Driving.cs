﻿using UnityEngine;
using System.Collections;

public class Driving : MonoBehaviour
{

    public float acceleration;
    public float maxVelocity;

    public float turnSpeed;



	// Use this for initialization
	void Start ()
    {
	    
	}

	// Update is called once per frame
	void Update ()
    {
        Vector3 vel = rigidbody.velocity;
        if(Input.GetKey(KeyCode.Z) && vel.magnitude < maxVelocity)
        {
            vel += transform.forward*acceleration * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.X) && vel.magnitude < maxVelocity/2)
        {
            vel += -transform.forward * acceleration * Time.deltaTime;
        }

        float turning = Input.GetAxis("Horizontal");
        transform.Rotate(new Vector3(0, turning * turnSpeed * Time.deltaTime, 0));

        rigidbody.velocity = vel;
	}
}
